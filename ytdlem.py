from __future__ import unicode_literals
from subprocess import call
import subprocess
import youtube_dl
import os

class MyLogger(object):
    def debug(self, msg):
        pass

    def warning(self, msg):
        pass

    def error(self, msg):
        print(msg)


def my_hook(d):
    if d['status'] == 'finished':
        print('Done downloading, now converting ...')


ydl_opts = {
    'logger': MyLogger(),
    'progress_hooks': [my_hook],
}
with open('yt.txt') as f:
    content = f.readlines()
    # you may also want to remove whitespace characters like `\n` at the end of each line
    # content = [x.strip for x in content]
    os.chdir("work") 
    for line in content:
        with youtube_dl.YoutubeDL(ydl_opts) as ydl:
            result = ydl.extract_info(line, download=True)
            if 'entries' in result:
                # Can be a playlist or a list of videos
                video = result['entries'][0]
            else:
                # Just a video
                video = result

