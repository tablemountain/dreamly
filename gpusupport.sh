#this this noise below, just run on GCLOUD w/o GPU support (cheap, mostly effective)

#considering you need gpu support, here's a mac osx outline of how to get that: 




https://gist.github.com/Mistobaan/dd32287eeb6859c6668d

#install bazel 
https://github.com/bazelbuild/bazel/releases/tag/0.4.5
chmod +x bazel-0.4.5-installer-darwin-x86_64.sh
./bazel-0.4.5-installer-darwin-x86_64.sh  --user
#then add to $PATH
/Users/timothybone/bin

git clone --recurse-submodules https://github.com/tensorflow/tensorflow
cd tensorflow; git checkout  r0.8
./configure

cp ./tensorflow/python/pywrap_tensorflow.py ./bazel-bin/tensorflow/python/pywrap_tensorflow_internal.py
git submodule update --init
bazel clean --expunge
bazel build -c opt --config=cuda --verbose_failures //tensorflow/tools/pip_package:build_pip_package
bazel-bin/tensorflow/tools/pip_package/build_pip_package ~/utils/tensorflow_pkg



PYTHON_BIN_PATH=/usr/local/bin/python CUDA_TOOLKIT_PATH="/usr/local/cuda" CUDNN_INSTALL_PATH="/usr/local/cuda" TF_UNOFFICIAL_SETTING=1 TF_NEED_CUDA=1 TF_CUDA_COMPUTE_CAPABILITIES="3.0" TF_CUDNN_VERSION="5" TF_CUDA_VERSION="7.5" TF_CUDA_VERSION_TOOLKIT=7.5 ./configure
bazel build -c opt  //tensorflow/tools/pip_package:build_pip_package

Download CUDA (7.5) (wants CUDA 8)
download CUDNN (5.1)
download xcode (7.2) 
brew tap caskroom/drivers

